# Spring Boot Oauth2 Example

#### Tutorial's Summary

How to create from scratch a REST service with Spring Boot. We'll secure it using the Oauth2 protocol, using JSON Web Tokens, or JWT. There are several interesting materials scattered on the web, however, after studying a lot of then, I believe that the theme could be examined a little further. Instead of simply showing how to configure the server, I'll try to briefly explain why such configuration is necessary.

#### To Build and Run

Clone this repo by this command : `git clone https://gitlab.com/hendisantika/springboot-oauth2.git`

Go to the cloned directory and run mvn spring-boot:run or build with your chosen IDE.

Run this project by this command : `mvn clean spring-boot:run`

#### Curl Commands
You should install [./JQ](https://stedolan.github.io/jq/) before running these Curl commands.

#### To get a new token
`curl trusted-app:secret@localhost:8080/oauth/token -d "grant_type=password&username=user&password=password" | jq`

Result :
```
{
  "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsib2F1dGgyX2lkIl0sInVzZXJfbmFtZSI6InVzZXIiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTQyNjMxODY1LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiNTllMTkwYTAtYTVlOS00ZTQ4LWIxMzItYzAyM2FkNDZlM2ExIiwiY2xpZW50X2lkIjoidHJ1c3RlZC1hcHAifQ.C6NPIXEwSSLk_2C6G7sMjaDq6X7djU2XFb4j84u_6ix9jnp8W3OgDtYuf_iFMmj0pNiYUj4dGfHY5JgqR-wi_wJDRu1bW7LtB_swuBVShs_pwmdbc5N_vnMNjpg89YFOLA_xhNMHcymlybt7iPtNry4YYtcWAKflektRbE5wWsDo8iw8B0BnzQVHW_sZpILD52ORvfZHG9uROqqUhBGBXx1-0s2U2yb-gDRS9jKeM40CWIxbQQuBXstlobIHYKxzIAJPste70FcQ1IEO6Etfx_8TpFPV1riSv96fCLpOnUHotoqZFn47T7N-PZE4oqOmTnLuVIxM5H-uREV8NgrBnw",
  "token_type": "bearer",
  "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsib2F1dGgyX2lkIl0sInVzZXJfbmFtZSI6InVzZXIiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiYXRpIjoiNTllMTkwYTAtYTVlOS00ZTQ4LWIxMzItYzAyM2FkNDZlM2ExIiwiZXhwIjoxNTQ1MTgwNjY1LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiOGQ1YzJlY2YtOWI0My00OWRiLTk2MDItMzk4ZTNlYTlhOWE1IiwiY2xpZW50X2lkIjoidHJ1c3RlZC1hcHAifQ.Kvrzf1kjqkSzuK13jlknCO--Albj-FSG6NI1_LwzukWcitPo1FiBt-Rk5o23f2Vnqkq_e-9uW6kNb6k6THQ2CONQH5BwerovH9hL7qW6IgpESzYplWcawaTbmdWvrBJ4jhyAJmoy2pdO1uU36iy7eL6c3eOF86CRQ6RA3UlTerAvgXC5_VX3FTi81jaNim5G8utfy31qCOjtDDAhlNDuBV4-A9zJ1Q8GlJsCi0NNN-8k_nPO_qNnkZCZfnOmqPW4D_lA8adANFc5knJW4668y0iKoddcNMGvXvQB8g4iR-gdQUR4VaZh3-zzdXEMbTp9gACUwHOhyADNUg7SKd8YPg",
  "expires_in": 43199,
  "scope": "read write",
  "jti": "59e190a0-a5e9-4e48-b132-c023ad46e3a1"
}
```

#### To get a refresh token
`curl trusted-app:secret@localhost:8080/oauth/token -d "grant_type=refresh_token&jti=[JTI]&refresh_token=[REFRESH_TOKEN]" | jq`

Result :
```
{
  "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsib2F1dGgyX2lkIl0sInVzZXJfbmFtZSI6InVzZXIiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTQyNjMyMTA3LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiZGEyZTk1MTItYWVjMy00YzQ3LWE1YTMtZjQ2MTgxODAwZmM2IiwiY2xpZW50X2lkIjoidHJ1c3RlZC1hcHAifQ.RuFYUwVB6t0FBMpQQ6SxE-mHqJiuhJRrpJo7_xIWYLKrnNIWnZBPHXYsMGQ-Y3DgbHuQGL9fb-IZW4xPfNpSZRpAszsik3iOARbNSZAnpOxiPH_Csb_CVT6oZXV_nlbkB01kL55wGdA3IjjVT7sHaiSy1doJO5IRGxFxjZBorkuHtUYTaGHkbxF0oEp39yAgHrLn5-jTB68oG3zkr8kpblAqnkHFQbhqVyNVW5prZJiVTSh9YLzh53oQMAHeA8_Xc39EafPVd7igY0BmTdlfaqzu63gyzKREagT-BNSe8bjEcE5DVCcNLBqsLZLStOgLiFnGdYYJVok3IqWgFwhXAA",
  "token_type": "bearer",
  "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsib2F1dGgyX2lkIl0sInVzZXJfbmFtZSI6InVzZXIiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiYXRpIjoiZGEyZTk1MTItYWVjMy00YzQ3LWE1YTMtZjQ2MTgxODAwZmM2IiwiZXhwIjoxNTQ1MTgwNjY1LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiOGQ1YzJlY2YtOWI0My00OWRiLTk2MDItMzk4ZTNlYTlhOWE1IiwiY2xpZW50X2lkIjoidHJ1c3RlZC1hcHAifQ.LpCE8zV4rUz8ZmF0Gw_PoclEZ5lJ_3Sb_EXPV0MC7pBLUz_6jOgDp7X4I2KC8DBxIRKv4co7gBrPUOsBvza6AanAO54gEer4SLQiq2q-xmUQalc87CdO_k0bAipdvEx-r1AEK7tWDLpmEkabSkNpp7VZO6PiqohNEM6gyQ7PSo857S2X9I7QfTD_CGPBdR2XObDkhiBBluLFwpVSp5Gs9_N5wIh06qwQPX66z2osUGd3P8AImcDbkMea-e1j0zKLiX0o6xP3qMk0YhEenIRnRzk5Hvsr7iL2sgU91I8MRp1DjeZ6mFdzmlFXvgLJzZxKxTDtgeJvDr0nPLM6dNwY2w",
  "expires_in": 43199,
  "scope": "read write",
  "jti": "da2e9512-aec3-4c47-a5a3-f46181800fc6"
}
```

#### To access a protected resource
`curl -H "Authorization: Bearer [ACCESS_TOKEN]" localhost:8080/api/hello`

Result :
```
{
  "msg": "Hello 'user'!"
}
```

#### Notes :
Using an asymmetric key

Note that, for the sake of simplicity, we used a symmetric key in the JwtAccessTokenConverter. This isn’t the best approach for a production environment. We should create an asymmetric key and use it to sign the converter. You can use Keytool to produce a key pair, running the following command on the `/src/main/resources/ directory`:

`keytool -genkeypair -alias mykeys -keyalg RSA -keypass mypass -keystore mykeys.jks -storepass mypass`
