package com.hendisantika.springbootoauth2.repository;

import com.hendisantika.springbootoauth2.entity.Account;
import org.springframework.data.repository.Repository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/11/18
 * Time: 07.21
 * To change this template use File | Settings | File Templates.
 */
public interface AccountRepo extends Repository<Account, Long> {
    Optional<Account> findByUsername(String username);

    Account save(Account account);

    int deleteAccountById(Long id);
}