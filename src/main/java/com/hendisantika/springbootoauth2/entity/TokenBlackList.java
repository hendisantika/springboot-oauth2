package com.hendisantika.springbootoauth2.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/11/18
 * Time: 07.20
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Data
public class TokenBlackList {

    @Id
    private String jti;
    private Long userId;
    private Long expires;
    private Boolean isBlackListed;

    public boolean isBlackListed() {
        return isBlackListed;
    }

    public void setBlackListed(boolean blackListed) {
        isBlackListed = blackListed;
    }

    public TokenBlackList(Long userId, String jti, Long expires) {
        this.jti = jti;
        this.userId = userId;
        this.expires = expires;
    }
}