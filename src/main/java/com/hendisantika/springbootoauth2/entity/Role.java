package com.hendisantika.springbootoauth2.entity;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/11/18
 * Time: 07.17
 * To change this template use File | Settings | File Templates.
 */
public enum Role {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_REGISTER,
    ROLE_TRUSTED_CLIENT,
    ROLE_CLIENT
}